# [Mantine](https://mantine.dev)

## Installation

default install with these [modules](https://mantine.dev/pages/basics/#mantine-packages):

- form
- dates
- notifications
- Rich Text Editor
- modals

```
yarn add dayjs @mantine/rte @mantine/notifications @mantine/modals @mantine/hooks @mantine/form @mantine/dates @mantine/core @mantine/next
```

## Configuration

1. Use `MantineProvider` in [`main.tsx`](./src/main.tsx) ([source](https://mantine.dev/theming/mantine-provider/))
2. Use `NotificationsProvider` in [`main.tsx`](./src/main.tsx) ([source](https://mantine.dev/others/notifications/))
