import {
  Button,
  Container,
  Divider,
  Group,
  LoadingOverlay,
  Title,
} from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import { useState } from "react";
import { Form } from "./components/Form";
import { PageModal } from "./components/Modal";
import { PageHeader } from "./components/PageHeader";
import { SideMenu } from "./components/SideMenu";

export const TestPage = () => {
  const [openFilters, setOpenFilters] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(false);

  return (
    <Container>
      <PageHeader />

      <LoadingOverlay visible={loading} />

      <Form />

      <Divider my="sm" />
      <Title order={2} sx={{ marginBottom: 20 }}>
        Page Controls
      </Title>

      <SideMenu open={openFilters} setOpen={setOpenFilters} />
      <PageModal open={openModal} setOpen={setOpenModal} />

      <Group position="center">
        <Button onClick={() => setOpenFilters(true)}>Open Drawer</Button>
        <Button onClick={() => setOpenModal(true)}>Open Modal</Button>
        <Button onClick={() => setLoading(true)}>Toggle loading state</Button>
        <Button
          onClick={() =>
            showNotification({
              message: "Notification",
            })
          }
        >
          Show Notification
        </Button>
      </Group>
    </Container>
  );
};
