import { Button, Group, Menu } from "@mantine/core";

export const Actions = () => {
  return (
    <Group>
      {/* SECONDARY ACTIONS */}
      <Menu
        control={
          <Button
            variant="outline"
            onClick={(e: any) => {
              e.stopPropagation();
            }}
          >
            Secondary actions
          </Button>
        }
      >
        <Menu.Label>Category</Menu.Label>
        <Menu.Item>Add</Menu.Item>
        <Menu.Item color="red">Delete</Menu.Item>
      </Menu>
      <Button
        onClick={(e: any) => {
          e.stopPropagation();
        }}
      >
        Add
      </Button>
    </Group>
  );
};
