import {
  Accordion,
  Button,
  Checkbox,
  createStyles,
  Group,
  InputWrapper,
  Select,
  Switch,
  Text,
  Textarea,
  TextInput,
} from "@mantine/core";
import { DatePicker, TimeInput } from "@mantine/dates";
import { Dropzone, DropzoneStatus, IMAGE_MIME_TYPE } from "@mantine/dropzone";
import { useForm } from "@mantine/form";
import RichTextEditor from "@mantine/rte";
import { useState } from "react";
import { Actions } from "./Actions";

const formStyles = createStyles(() => ({
  input: {
    marginBottom: 15,
  },
}));

export const Form = () => {
  const [rteValue, onChangeRte] = useState("");

  const { classes } = formStyles();
  const form = useForm({
    initialValues: {
      name: "",
      email: "",
      date: new Date(),
      time: null,
      termsOfService: false,
    },

    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
    },
  });

  return (
    <>
      {/* FORM */}
      <form onSubmit={form.onSubmit((values) => console.log(values))}>
        <Accordion multiple initialItem={0}>
          <Accordion.Item
            label={
              <Group position="apart">
                <Text>Input controls</Text>
                <Actions />
              </Group>
            }
          >
            <TextInput
              required
              className={classes.input}
              label="Airport name"
              placeholder="your name"
              {...form.getInputProps("name")}
            />
            <TextInput
              required
              className={classes.input}
              label="email"
              placeholder="your@email.com"
              {...form.getInputProps("email")}
            />

            <Select
              label="Your favorite framework/library"
              placeholder="Pick one"
              searchable
              maxDropdownHeight={400}
              nothingFound="Nothing found"
              filter={(value, item) =>
                item?.label
                  ?.toLowerCase()
                  .includes(value.toLowerCase().trim()) || false
              }
              data={[
                { value: "react", label: "React" },
                { value: "ng", label: "Angular" },
                { value: "svelte", label: "Svelte" },
                { value: "vue", label: "Vue" },
              ]}
            />
            <Checkbox
              mt="md"
              mb="md"
              label="My checkbox"
              {...form.getInputProps("termsOfService", { type: "checkbox" })}
            />

            <Switch mt="md" mb="md" label="My radio" />

            <Textarea
              placeholder="Your comment"
              label="Your comment"
              required
            />

            <InputWrapper
              mt="md"
              mb="md"
              id="input-demo"
              required
              label="Rich Text Editor"
              description="Please enter your credit card information, we need some money"
              error="this is a custom error message"
            >
              <RichTextEditor value={rteValue} onChange={onChangeRte} />
            </InputWrapper>
          </Accordion.Item>
        </Accordion>

        <Accordion multiple initialItem={0}>
          <Accordion.Item label="Date controls">
            <TimeInput
              className={classes.input}
              label="What time is it now?"
              {...form.getInputProps("time")}
            />

            <DatePicker
              className={classes.input}
              placeholder="Pick date"
              label="Event date"
              required
              {...form.getInputProps("date")}
            />
          </Accordion.Item>
        </Accordion>

        <Accordion multiple initialItem={0}>
          <Accordion.Item label="File drag & drop">
            <Dropzone
              onDrop={(files) => console.log("accepted files", files)}
              onReject={(files) => console.log("rejected files", files)}
              maxSize={3 * 1024 ** 2}
              accept={IMAGE_MIME_TYPE}
            >
              {(status) => dropzoneChildren(status)}
            </Dropzone>
          </Accordion.Item>
        </Accordion>

        <Group mt="md">
          <Button type="submit">Submit</Button>
        </Group>
      </form>
    </>
  );
};

export const dropzoneChildren = (status: DropzoneStatus) => (
  <Group
    position="center"
    spacing="xl"
    style={{ minHeight: 220, pointerEvents: "none" }}
  >
    <div>
      <Text size="xl" inline>
        Drag import file here
      </Text>
      <Text size="sm" color="dimmed" inline mt={7}>
        Attach as many files as you like, each file should not exceed 5mb
      </Text>
    </div>
  </Group>
);
