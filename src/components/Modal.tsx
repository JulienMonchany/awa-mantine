import { Button, Modal, Text } from "@mantine/core";
import { Dispatch, useState } from "react";

export const PageModal = ({
  open,
  setOpen,
}: {
  open: boolean;
  setOpen: Dispatch<boolean>;
}) => {
  const [openSubModal, toggleSubModal] = useState(false);

  return (
    <Modal
      opened={open}
      onClose={() => setOpen(false)}
      title="Introduce yourself!"
    >
      <Text>This is a modal</Text>
      <Button onClick={() => toggleSubModal(true)}>Open sub modal</Button>
      <PageModal open={openSubModal} setOpen={toggleSubModal} />
    </Modal>
  );
};
