import { Group, Title, Badge, Breadcrumbs, Anchor, Stack } from "@mantine/core";
import { Actions } from "./Actions";

const items = [
  { title: "Mantine", href: "#" },
  { title: "Mantine hooks", href: "#" },
  { title: "use-id", href: "#" },
].map((item, index) => (
  <Anchor href={item.href} key={index}>
    {item.title}
  </Anchor>
));

export const PageHeader = () => {
  return (
    <Stack spacing="sm">
      <Breadcrumbs>{items}</Breadcrumbs>
      <Group align="center" position="apart" sx={{ marginBottom: 20 }}>
        <Group>
          <Title order={1} sx={{ display: "inline-block" }}>
            New Airport
          </Title>
          <Badge sx={{ marginTop: 2 }}>Badge</Badge>
        </Group>
        <Actions />
      </Group>
    </Stack>
  );
};
