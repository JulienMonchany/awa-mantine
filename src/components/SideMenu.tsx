import { Accordion, Drawer, TextInput } from "@mantine/core";
import { Dispatch } from "react";

export const SideMenu = ({
  open,
  setOpen,
}: {
  open: boolean;
  setOpen: Dispatch<boolean>;
}) => {
  return (
    <Drawer
      opened={open}
      onClose={() => setOpen(false)}
      title="Filters"
      padding="xl"
      size="xl"
      position="right"
    >
      <Accordion multiple initialItem={0}>
        <Accordion.Item label="Date controls">
          <TextInput label="Input" placeholder="input" />
        </Accordion.Item>
      </Accordion>
    </Drawer>
  );
};
