import { Header, Group, useMantineTheme, Text, ThemeIcon } from "@mantine/core";
import { IoIosPaperPlane } from "react-icons/io";

export const MainHeader = () => {
  const theme = useMantineTheme();
  return (
    <Header height={60}>
      <Group sx={{ height: "100%" }} px={20} position="apart">
        <Group sx={{ height: "100%" }}>
          <IoIosPaperPlane color={theme.primaryColor} size={30} />
          <Text>AWA</Text>
        </Group>
        <Text>Okio</Text>
      </Group>
    </Header>
  );
};
