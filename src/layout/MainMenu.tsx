import { Group, MantineColor, Text, UnstyledButton } from "@mantine/core";

type SideMenuItem = {
  color: MantineColor;
  label: string;
};

const links: SideMenuItem[] = [
  { color: "blue", label: "Human Ressources" },
  { color: "teal", label: "Skill Monitoring" },
  { color: "violet", label: "Fleet" },
  { color: "green", label: "Production" },
  { color: "yellow", label: "Workshop" },
  { color: "orange", label: "Financial" },
  { color: "gray", label: "Logistic" },
  { color: "red", label: "EDamage Assesment" },
];

export const MainMenu = () => (
  <>
    {links.map((link) => (
      <SideMenuItem {...link} key={link.label} />
    ))}
  </>
);

const SideMenuItem = ({ color, label }: SideMenuItem) => {
  return (
    <UnstyledButton
      sx={(theme) => ({
        display: "block",
        width: "100%",
        padding: theme.spacing.xs,
        borderRadius: theme.radius.sm,

        "&:hover": {
          backgroundColor: theme.colors.gray[0],
        },
      })}
    >
      <Group>
        <Text size="sm">{label}</Text>
      </Group>
    </UnstyledButton>
  );
};
