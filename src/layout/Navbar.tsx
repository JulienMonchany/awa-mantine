import { Divider, Navbar, useMantineTheme } from "@mantine/core";
import { MainMenu } from "./MainMenu";
import { User } from "./User";

export const NavBar = () => {
  const theme = useMantineTheme();
  return (
    <Navbar
      width={{ base: 300 }}
      height="calc(100vh - 60px)"
      p="xs"
      style={{ background: theme.colors.gray[1] }}
    >
      <Navbar.Section grow mt="xs">
        <MainMenu />
      </Navbar.Section>
      <Divider my="sm" />
      <Navbar.Section>
        <User />
      </Navbar.Section>
    </Navbar>
  );
};
