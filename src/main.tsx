import {
  AppShell,
  Group,
  Header,
  MantineProvider,
  ScrollArea,
  Text,
} from "@mantine/core";
import { NotificationsProvider } from "@mantine/notifications";
import { SpotlightAction, SpotlightProvider } from "@mantine/spotlight";
import React from "react";
import ReactDOM from "react-dom/client";
import { TbDashboard, TbFileText, TbHome, TbSearch } from "react-icons/tb";
import { MainHeader } from "./layout/MainHeader";
import { NavBar } from "./layout/Navbar";
import { TestPage } from "./TestPage";

const actions: SpotlightAction[] = [
  {
    title: "Users",
    group: "Human ressources",
    description: "Get to home page",
    onTrigger: () => console.log("Home"),
    icon: <TbHome size={18} />,
  },
  {
    title: "Groups",
    group: "Human ressources",
    description: "Get full information about current system status",
    onTrigger: () => console.log("Groups"),
    icon: <TbDashboard size={18} />,
  },
  {
    title: "My tasks",
    group: "Production",
    description: "Visit documentation to lean more about all features",
    onTrigger: () => console.log("My tasks"),
    icon: <TbFileText size={18} />,
  },
];

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <MantineProvider
      theme={{
        fontFamily: `"Helvetica Neue", Helvetica, Arial, sans-serif`,
        spacing: { xs: 15, sm: 20, md: 25, lg: 30, xl: 40 },
      }}
    >
      <SpotlightProvider
        actions={actions}
        searchIcon={<TbSearch size={18} />}
        searchPlaceholder="Search..."
        shortcut={["mod + K"]}
        nothingFoundMessage="Nothing found..."
      >
        <NotificationsProvider position="top-right">
          <AppShell padding="md" navbar={<NavBar />} header={<MainHeader />}>
            <ScrollArea style={{ height: "calc(100vh - 110px)" }}>
              <TestPage />
            </ScrollArea>
          </AppShell>
        </NotificationsProvider>
      </SpotlightProvider>
    </MantineProvider>
  </React.StrictMode>
);
